class Person {
    constructor (name, age, salary, sex) {
        this.name = name
        this.age = age
        this.salary = salary
        this.sex = sex
    }

    static swap (arr, i, j) {
        var temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
    }

    static sortHelper (arr, field, start, end) {

        if (start >= end) return

        let temp = 0
        for (let i = start; i < end; i++) {
            if (arr[i][field] < arr[start][field]) temp += 1
        }

        let pivot = start+temp
        this.swap(arr, start, pivot)

        let i = 0, j = arr.length - 1
        while (i < pivot & pivot < j) {
            if (arr[i][field] >= arr[pivot][field]) {
                if (arr[j][field] < arr[pivot][field]) {
                    this.swap(arr, i, j)
                } else {
                    j -= 1
                }
            } else {
                i += 1
            }
        }

        this.sortHelper(arr, field, start, pivot)
        this.sortHelper(arr, field, pivot+1, end)
    }

    static sort (arr, field, order) {
        this.sortHelper(arr, field, 0, arr.length)
        if (order == 'asc') return arr
        return arr.reverse()
    }
}
